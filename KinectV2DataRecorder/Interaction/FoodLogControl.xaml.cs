﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TabelConnect.Interaction
{
    /// <summary>
    /// FoodLogControl.xaml の相互作用ロジック
    /// </summary>
    public partial class FoodLogControl : UserControl
    {
        public FoodLogControl()
        {
            InitializeComponent();
        }

        public void SetFoodLogImages(IEnumerable<string> foodLogImgPaths)
        {
            foreach (var foodLogImgPath in foodLogImgPaths)
            {
                this.foodLogListBox.Items.Add(new FoodLogItem(foodLogImgPath) { Height = this.Height / 3 - 30, Width = this.Width / 3 -10 });
            }
        }
    }
}
