﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TabelConnect.Interaction
{
    /// <summary>
    /// FoodInfoPopup.xaml の相互作用ロジック
    /// </summary>
    public partial class FoodInfoPopup : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        Action<string> notifyPropertyChange;

        double calorie;
        public double Calorie { get { return calorie; } set{ calorie = value; notifyPropertyChange("Calorie"); }}
        double carbohydrates;
        public double Carbohydrates { get { return carbohydrates; } set { carbohydrates = value; notifyPropertyChange("Carbohydrates"); } }
        double protein;
        public double Protein { get { return protein; } set { protein = value; notifyPropertyChange("Protein"); } }
        double fat;
        public double Fat { get { return fat; } set { fat = value; notifyPropertyChange("Fat"); } }

        public FoodInfoPopup()
        {
            InitializeComponent();
            notifyPropertyChange = (property) =>
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(property));
            };
        }

    }
}
