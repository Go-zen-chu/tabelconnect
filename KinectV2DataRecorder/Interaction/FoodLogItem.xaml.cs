﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TabelConnect.Interaction
{
    /// <summary>
    /// FoodLogControl.xaml の相互作用ロジック
    /// </summary>
    public partial class FoodLogItem : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        Action<string> notifyPropertyChange;

        DateTime loggedTime;
        public DateTime LoggedTime
        {
            get { return loggedTime; }
            set
            {
                loggedTime = value;
                notifyPropertyChange("DateStr");
                notifyPropertyChange("TimeStr");
            }
        }
        public string DateStr
        {
            get
            {
                return LoggedTime.ToShortDateString(); 
            }
        }
        public string TimeStr
        {
            get
            {
                return LoggedTime.ToShortTimeString();
            }
        }

        public FoodLogItem(string foodLogImgPath)
        {
            InitializeComponent();
            notifyPropertyChange = (property) =>
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(property));
            };

            var fileName = System.IO.Path.GetFileNameWithoutExtension(foodLogImgPath);
            var dt = DateTime.MinValue;
            DateTime.TryParseExact(fileName, Properties.Resources.DateTimeFormatText, null, System.Globalization.DateTimeStyles.None, out dt);
            LoggedTime = dt;
            this.foodLogImage.Source = new WriteableBitmap(new BitmapImage(new Uri(foodLogImgPath)));

            //TODO
            var rand = new Random();
            this.foodInfoPopup.Calorie =1000 + 200 * rand.NextDouble();
            this.foodInfoPopup.Carbohydrates = 100 + 40 * rand.NextDouble();
            this.foodInfoPopup.Protein = 100 + 40 * rand.NextDouble();
            this.foodInfoPopup.Fat = 140 + 20 * rand.NextDouble();
        }

    }
}
