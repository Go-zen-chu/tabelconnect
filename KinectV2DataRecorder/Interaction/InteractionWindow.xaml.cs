﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace TabelConnect.Interaction
{
    /// <summary>
    /// InteractionWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class InteractionWindow : Window
    {
        Rect interactionArea;
        const double PRJ_DENSE_X = 1024;
        const double PRJ_DENSE_Y = 768;
        public bool IsTakingPicture { get; set; }

        public InteractionWindow()
        {
            InitializeComponent();
            IsTakingPicture = false; // for picture timer
            this.interactionArea = new Rect(Properties.Settings.Default.Mapping_TopLeft, Properties.Settings.Default.Mapping_BottomRight);
            mainCanvas.Width = interactionArea.Width;
            mainCanvas.Height = interactionArea.Height;
            //mainCanvas.LayoutTransform = new ScaleTransform(-1, 1);// 取得データや画像が逆なので、左右反転させる
        }

        public DispatcherOperation CreateTakePictureTimer(Rect foodCaptureArea)
        {
            var convertedCaptureArea = convertImgRectToPrjPoint(foodCaptureArea);
            return this.Dispatcher.BeginInvoke((Func<DispatcherTimer>)(()=>{
                // interaction window に画像を設定
                //var foodAreaImg = new Image() { Source = new BitmapImage(new Uri(@"Images/photoZone.png", UriKind.Relative)), Width = foodCaptureArea.Width, Height = foodCaptureArea.Height };
                var foodAreaRect = new System.Windows.Shapes.Rectangle() { Width = convertedCaptureArea.Width, Height = convertedCaptureArea.Height, Stroke = Brushes.LightGreen, StrokeThickness = 5};
                foodAreaRect.RenderTransform = new TranslateTransform(convertedCaptureArea.Left, convertedCaptureArea.Top);
                mainCanvas.Children.Add(foodAreaRect);

                var timer = new DispatcherTimer();
                timer.Tag = null;
                timer.Interval = TimeSpan.FromSeconds(1);
                int countDown = 4; // take picture after 4 sec
                timer.Tick += (s, e) =>
                {
                    var player = countDown > 1 ? new SoundPlayer(Properties.Resources.countDown) : new SoundPlayer(Properties.Resources.shutter);
                    player.Play();
                    player.Dispose();
                    if (countDown == 1)
                    {
                        timer.Tag = foodCaptureArea; // must put actual food capture area not on this window
                        timer.Stop();
                    }
                    else if(countDown == 2)
                    {
                        // remove photo zone before taking picture so that it would not remain in picture
                        mainCanvas.Children.Remove(foodAreaRect);
                    }
                    countDown--;
                };
                timer.Start();
                return timer;
            }));
        }

        public void DrawDishes(List<DishFinder.Dish> detectedDishes)
        {
            this.Dispatcher.BeginInvoke((Action)(() =>
            {
                foreach (var dish in detectedDishes)
                {
                    var prjRect = convertImgRectToPrjPoint(dish.BoundingRect);
                    var rect = new Rectangle() { Stroke = Brushes.White, StrokeThickness = 4, Width = prjRect.Width, Height = prjRect.Height };
                    rect.RenderTransform = new TranslateTransform(prjRect.Left, prjRect.Top);
                    mainCanvas.Children.Add(rect);
                }
                // DispatcherTimer only works in UI Thread
                var timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromSeconds(5);
                timer.Tick += (s, e) =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() => mainCanvas.Children.Clear()));
                    timer.Stop();
                };
                timer.Start();
            }));
        }

        public void ReceiveDishImage(Rect foodCaptureArea)
        {
            var convertedCaptureArea = convertImgRectToPrjPoint(foodCaptureArea);
            this.Dispatcher.BeginInvoke((Action)(() =>
            {
                var takenImagePath = Directory.EnumerateFiles(System.IO.Path.Combine(Properties.Settings.Default.AppDataRootPath, "TakenFood"), "*.jpg", SearchOption.AllDirectories).First();
                var receivedFoodImage = new Image() { Source = new BitmapImage(new Uri(takenImagePath)) };
                receivedFoodImage.RenderTransform = new TranslateTransform(convertedCaptureArea.Left, convertedCaptureArea.Top);
                mainCanvas.Children.Add(receivedFoodImage);
                // DispatcherTimer only works in UI Thread
                var timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromSeconds(10);
                timer.Tick += (s, e) =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() => mainCanvas.Children.Clear()));
                    timer.Stop();
                };
                timer.Start();
            }));
        }

        public void ShowCalories(List<DishFinder.Dish> detectedDishes, List<IplImage> dishImgs){
            this.Dispatcher.BeginInvoke((Action)(() =>
            {
                var rand = new Random();
                for(int dishIdx = 0; dishIdx < detectedDishes.Count; dishIdx++){
                    var dish = detectedDishes[dishIdx];
                    var dishImg = dishImgs[dishIdx];
                    dishImg.Dispose();
                    var prjRect = convertImgRectToPrjPoint(dish.BoundingRect);
                    //TODO
                    var popupHeight = interactionArea.Height / 6;
                    var foodInfo = new FoodInfoPopup() { Height = popupHeight, Width = 1.5 * popupHeight, Calorie = 900 + 200 * rand.NextDouble(), Carbohydrates = 10 + 30 * rand.NextDouble(), Protein = 10 + 30 * rand.NextDouble(), Fat = 20 + 30 * rand.NextDouble() };
                    //var foodInfo = new Rectangle() { Stroke = Brushes.White, StrokeThickness = 4, Width = prjRect.Width, Height = prjRect.Height };
                    foodInfo.RenderTransform = new TranslateTransform(prjRect.Left, prjRect.Bottom + 20);
                    mainCanvas.Children.Add(foodInfo);
                }
                // DispatcherTimer only works in UI Thread
                var timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromSeconds(5);
                timer.Tick += (s, e) =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() => mainCanvas.Children.Clear()));
                    timer.Stop();
                };
                timer.Start();
            }));
        }

        public void ShowFoodLog(IEnumerable<string> foodLogImgPaths)
        {
            this.Dispatcher.BeginInvoke((Action)(() =>
            {
                var foodLogControl = new FoodLogControl() { Height = this.ActualHeight, Width = this.ActualWidth };
                foodLogControl.SetFoodLogImages(foodLogImgPaths);
                mainCanvas.Children.Add(foodLogControl);
                // DispatcherTimer only works in UI Thread
                var timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromSeconds(10);
                timer.Tick += (s, e) =>
                {
                    this.Dispatcher.BeginInvoke((Action)(() => mainCanvas.Children.Clear()));
                    timer.Stop();
                };
                timer.Start();
            }));
        }

        Point convertImgPointToPrjPoint(Point imgPoint)
        {
            var prjX = (interactionArea.Right - imgPoint.X) *PRJ_DENSE_X / interactionArea.Width;
            var prjY = (imgPoint.Y - interactionArea.Top) *PRJ_DENSE_Y / interactionArea.Height;
            return new Point(prjX, prjY);
        }

        Rect convertImgRectToPrjPoint(Rect imgRect)
        {
            // 左右反転であるから右上と左下を取得
            return new Rect(convertImgPointToPrjPoint(imgRect.TopRight), convertImgPointToPrjPoint(imgRect.BottomLeft));
        }

        private void fullScreenButton_Click(object sender, RoutedEventArgs e)
        {
            this.WindowStyle = System.Windows.WindowStyle.None;
            this.WindowState = System.Windows.WindowState.Maximized;
            fullScreenButton.Visibility = System.Windows.Visibility.Collapsed;
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
            mainCanvas.Height = this.ActualHeight;
            mainCanvas.Width = this.ActualWidth;
        }
    }
}
