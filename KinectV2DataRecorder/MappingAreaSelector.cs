﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace TabelConnect
{
    public class MappingAreaSelector : Canvas
    {
        int clickCount = 0;
        Point[] cornerPos = new Point[4];
        Line[,] crosses = new Line[4, 2];
        const int LINE_LENGTH = 20;

        public MappingAreaSelector()
        {
            this.Background = new SolidColorBrush(Color.FromRgb(255, 255, 255)) { Opacity = 0.1 }; //透明ではクリックイベントが発生しないため、ぎりぎり透明にする
            this.MouseLeftButtonDown += MappingAreaSelector_MouseLeftButtonDown;
        }

        void MappingAreaSelector_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var cornerIdx = clickCount % 4;
            var pos = e.GetPosition(this);
            cornerPos[cornerIdx] = pos;
            if (crosses[cornerIdx, 0] != null && crosses[cornerIdx, 1] != null)
            {
                this.Children.Remove(crosses[cornerIdx, 0]);
                this.Children.Remove(crosses[cornerIdx, 1]);
            }
            var vLine = new Line() { X1 = pos.X, X2 = pos.X, Y1 = pos.Y - LINE_LENGTH / 2, Y2 = pos.Y + LINE_LENGTH / 2, Stroke = Brushes.LightGreen, StrokeThickness = 3 };
            var hLine = new Line() { X1 = pos.X - LINE_LENGTH / 2, X2 = pos.X + LINE_LENGTH / 2, Y1 = pos.Y, Y2 = pos.Y, Stroke = Brushes.LightGreen, StrokeThickness = 3 };
            crosses[cornerIdx, 0] = vLine;
            crosses[cornerIdx, 1] = hLine;

            this.Children.Add(vLine);
            this.Children.Add(hLine);

            clickCount++;
        }

        public void SaveCornerPosition()
        {
            Properties.Settings.Default.Mapping_TopLeft = cornerPos[0];
            Properties.Settings.Default.Mapping_TopRight = cornerPos[1];
            Properties.Settings.Default.Mapping_BottomLeft = cornerPos[2];
            Properties.Settings.Default.Mapping_BottomRight = cornerPos[3];
            Properties.Settings.Default.Save();
        }
    }
}
