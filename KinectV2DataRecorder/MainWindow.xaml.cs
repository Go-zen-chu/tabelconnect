﻿using IdentifyUserFromTop;
using TabelConnect.Interaction;
using Microsoft.Kinect;
using Microsoft.Speech.AudioFormat;
using Microsoft.Speech.Recognition;
using OpenCvSharp;
using OpenCvSharp.Extensions;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Media;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Threading;

namespace TabelConnect
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private enum InteractionMode
        {
            None, ShowCalorie, ShowFoodLog, SearchFood, TransportFood, ReceiveFood
        }

        // INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        public event PropertyChangedEventHandler PropertyChanged;
        private KinectSensor kinectSensor = null;
        // Size of the RGB pixel in the bitmap
        private readonly int bytesPerPixel = (PixelFormats.Bgr32.BitsPerPixel + 7) / 8;
        public const ushort KINECT_VALID_MAX_DEPTH = 10000;
        // Lool up table for converting depth to color
        private readonly byte[] lut_g = new byte[KINECT_VALID_MAX_DEPTH];
        private readonly byte[] lut_b = new byte[KINECT_VALID_MAX_DEPTH];

        private readonly string takenFoodImageSavingDirPath;
        private readonly string foodLogImageSavingDirPath;


        #region body part classification
        private BodyPartsClassifier classifier = null;
        private int[,] offsetVectors = null;
        #endregion

        #region Recording related variables
        private bool isRecording = false;
        private DateTime prevRecordedTime = DateTime.MinValue;
        private int recordingInterval_ms = 500; // = 500ms
        private string recordSessionDirPath;
        #endregion

        // for calculating FPS
        private DateTime prevRenderedTime = DateTime.MinValue;
        // Coordinate mapper to map one type of point to another
        private CoordinateMapper coordinateMapper = null;
        // Reader for depth/color/body index frames
        private MultiSourceFrameReader multiFrameSourceReader = null;

        #region Depth related variables
        // Description of the data contained in the depth frame
        private FrameDescription depthFrameDescription = null;
        // Intermediate storage for receiving depth frame data from the sensor
        private ushort[] depthFrameData = null;
        // depth mapped points
        private DepthSpacePoint[] depthPoints = null;
        private short[] mappedDepthData = null;
        #endregion

        private DateTime prePlaneFindTime = DateTime.MinValue;
        private TimeSpan findPlaneInterval = TimeSpan.FromMilliseconds(300);
        private PlaneFinder.AveragePlane avePlane = null;
        bool isSkypeLoggedIn = false;
        private Queue<int> preDetectedDishNum = new Queue<int>();
        private int maxDishNum = 0;
        private DateTime mealSessionStartDateTime;
        //private List<DishFinder.Dish> preDetectedDishes = null;
        private Color deskColor;

        private InteractionMode interactionState = InteractionMode.None;
        private InteractionWindow interactionWindow;

        #region Audio related

        /// <summary>
        /// Stream for 32b-16b conversion.
        /// </summary>
        private KinectAudioStream convertStream = null;
        /// <summary>
        /// Speech recognition engine using audio data from Kinect.
        /// </summary>
        private SpeechRecognitionEngine speechEngine = null;
        #endregion

        #region Properties
        private WriteableBitmap colorBitmap = null;
        public ImageSource ColorImageSource
        {
            get
            {
                return this.colorBitmap;
            }
        }

        private WriteableBitmap bodyPartsEstimationBitmap = null;
        public ImageSource BodyPartsEstimationImageSource
        {
            get { return this.bodyPartsEstimationBitmap; }
        }

        private int kinect_fps = 0;
        public int FPS
        {
            get { return kinect_fps; }
            set
            {
                if (this.kinect_fps != value)
                {
                    this.kinect_fps = value;
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("FPS"));
                    }
                }
            }
        }

        private string statusText = null;
        // Gets or sets the current status text to display
        public string StatusText
        {
            get
            {
                return this.statusText;
            }
            set
            {
                if (this.statusText != value)
                {
                    this.statusText = value;

                    // notify any bound elements that the text has changed
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("StatusText"));
                    }
                }
            }
        }

        private short averageDepth = 0;
        public short AverageDepth
        {
            get { return averageDepth; }
            set
            {
                averageDepth = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs("AverageDepth"));
                }
            }
        }

        #endregion


        // CONSTRUCTER
        public MainWindow()
        {
            #region Path Setting

            if (Directory.Exists(Properties.Settings.Default.AppDataRootPath) == false)
            {
                var fbd = createDataExportPathDialog();
                if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    Properties.Settings.Default.AppDataRootPath = fbd.SelectedPath;
                    Properties.Settings.Default.Save();
                }
                else
                {
                    this.Close();
                }
            }
            takenFoodImageSavingDirPath = Path.Combine(Properties.Settings.Default.AppDataRootPath, Properties.Resources.DirNameTakenFood);
            Directory.CreateDirectory(takenFoodImageSavingDirPath);
            foodLogImageSavingDirPath = Path.Combine(Properties.Settings.Default.AppDataRootPath, Properties.Resources.DirNameFoodLog);
            Directory.CreateDirectory(foodLogImageSavingDirPath);
            #endregion

            // get the kinectSensor object
            this.kinectSensor = KinectSensor.GetDefault();
            this.multiFrameSourceReader = this.kinectSensor.OpenMultiSourceFrameReader(FrameSourceTypes.Depth | FrameSourceTypes.Color);
            // wire handler for frames arrival
            this.multiFrameSourceReader.MultiSourceFrameArrived += this.Reader_MultiSourceFrameArrived;
            // get the coordinate mapper
            this.coordinateMapper = this.kinectSensor.CoordinateMapper;

            #region Color initialization
            // create the colorFrameDescription from the ColorFrameSource using Bgra format
            var colorFrameDescription = this.kinectSensor.ColorFrameSource.CreateFrameDescription(ColorImageFormat.Bgra);
            int colorWidth = colorFrameDescription.Width;
            int colorHeight = colorFrameDescription.Height;
            // create the bitmap to display
            this.colorBitmap = new WriteableBitmap(colorWidth, colorHeight, 96.0, 96.0, PixelFormats.Bgr32, null);
            #endregion

            #region Depth initialization
            // get FrameDescription from DepthFrameSource
            this.depthFrameDescription = this.kinectSensor.DepthFrameSource.FrameDescription;
            // allocate space to put the pixels being received and converted
            this.depthFrameData = new ushort[depthFrameDescription.Width * depthFrameDescription.Height];
            this.depthPoints = new DepthSpacePoint[colorWidth * colorHeight];
            this.mappedDepthData = new short[colorWidth * colorHeight];
            #endregion

            initializeVoiceRecognition();

            // set IsAvailableChanged event notifier
            this.kinectSensor.IsAvailableChanged += this.Sensor_IsAvailableChanged;
            // open the sensor
            this.kinectSensor.Open();
            // set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText : Properties.Resources.NoSensorStatusText;

            // use the window object as the view model in this simple example
            this.DataContext = this;
            // initialize the components (controls) of the window
            this.InitializeComponent();

            if (Directory.Exists(Properties.Settings.Default.AppDataRootPath) == false)
                startRecordingToggleButton.IsEnabled = false;

            // Create LUT
            for (ushort depth = 0; depth < KINECT_VALID_MAX_DEPTH; depth++)
            {
                lut_g[depth] = (byte)(depth / 256);
                lut_b[depth] = (byte)(depth % 256);
            }
            loadClassifier();

            interactionWindow = new InteractionWindow();
            interactionWindow.Show();
        }
        /// <summary>
        /// Handles the event which the sensor becomes unavailable (E.g. paused, closed, unplugged).
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Sensor_IsAvailableChanged(object sender, IsAvailableChangedEventArgs e)
        {
            // on failure, set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText : Properties.Resources.NoSensorStatusText;
        }
        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (this.multiFrameSourceReader != null)
            {
                // MultiSourceFrameReder is IDisposable
                this.multiFrameSourceReader.Dispose();
                this.multiFrameSourceReader = null;
            }
            stopVoiceRecognition();

            if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }
        }

        /// <summary>
        /// Handles the depth/color/body index frame data arriving from the sensor
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="eArgs">event arguments</param>
        private void Reader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs eArgs)
        {
            int colorWidth = 0, colorHeight = 0;
            int depthWidth = 0, depthHeight = 0;

            bool colorFrameProcessed = false;
            bool depthFrameProcessed = false;

            MultiSourceFrame multiSourceFrame = eArgs.FrameReference.AcquireFrame();
            if (multiSourceFrame == null) return;
            
            #region Prepare color image
            using (ColorFrame colorFrame = multiSourceFrame.ColorFrameReference.AcquireFrame())
            {
                if (colorFrame != null)
                {
                    FrameDescription colorFrameDescription = colorFrame.FrameDescription;
                    using (KinectBuffer colorBuffer = colorFrame.LockRawImageBuffer())
                    {
                        this.colorBitmap.Lock();
                        colorWidth = colorFrameDescription.Width;
                        colorHeight = colorFrameDescription.Height;
                        // verify data and write the new color frame data to the display bitmap
                        if ((colorWidth == this.colorBitmap.PixelWidth) && (colorHeight == this.colorBitmap.PixelHeight))
                        {
                            colorFrame.CopyConvertedFrameDataToIntPtr(
                                this.colorBitmap.BackBuffer,
                                (uint)(colorWidth * colorHeight * 4),
                                ColorImageFormat.Bgra);
                            // specify where to copy in bmp
                            this.colorBitmap.AddDirtyRect(new Int32Rect(0, 0, this.colorBitmap.PixelWidth, this.colorBitmap.PixelHeight));
                            // flag on
                            colorFrameProcessed = true;
                        }
                        this.colorBitmap.Unlock();
                    }
                }
            }
            #endregion

            #region Obtain Depth data
            // Frame Acquisition should always occur first when using multiSourceFrameReader
            using (DepthFrame depthFrame = multiSourceFrame.DepthFrameReference.AcquireFrame())
            {
                if (depthFrame != null)
                {
                    FrameDescription depthFrameDescription = depthFrame.FrameDescription;
                    depthWidth = depthFrameDescription.Width;
                    depthHeight = depthFrameDescription.Height;

                    if ((depthWidth * depthHeight) == this.depthFrameData.Length)
                    {
                        depthFrame.CopyFrameDataToArray(this.depthFrameData);
                        // flag on
                        depthFrameProcessed = true;
                    }
                }
            }
            #endregion

            if (depthFrameProcessed && colorFrameProcessed)
            {
                // calculate fps
                var currentTime = DateTime.Now;
                this.FPS = (int)(1000 / (currentTime - prevRenderedTime).TotalMilliseconds);

                #region Prepare Depth image
                // map depth to color image
                this.coordinateMapper.MapColorFrameToDepthSpace(this.depthFrameData, this.depthPoints);
                Parallel.For(0, colorHeight, row =>
                {
                    int colorIdx = colorWidth * row;
                    for (int col = 0; col < colorWidth; ++col)
                    {
                        DepthSpacePoint depthPoint = this.depthPoints[colorIdx];
                        mappedDepthData[colorIdx] = 0;
                        if (!float.IsNegativeInfinity(depthPoint.X) && !float.IsNegativeInfinity(depthPoint.Y))
                        {
                            // make sure the depth pixel maps to a valid point in color space
                            int depthX = (int)(depthPoint.X + 0.5f);
                            int depthY = (int)(depthPoint.Y + 0.5f);
                            if ((depthX >= 0) && (depthX < depthWidth) && (depthY >= 0) && (depthY < depthHeight))
                            {
                                int depthIndex = (depthY * depthWidth) + depthX;
                                var depth = depthFrameData[depthIndex];
                                // insert color image mapped depth value
                                mappedDepthData[colorIdx] = (short)depth;
                            }
                        }
                        colorIdx++;
                    }
                });
                #endregion

                #region Find Dishes

                if (showPlaneCheckBox.IsChecked.Value && mappedDepthData != null && (currentTime - prePlaneFindTime) > findPlaneInterval)
                {
                    prePlaneFindTime = currentTime;
                    #region View Table Points

                    //if (currentTime.Millisecond / 100 == 0) // 描画の関係上、あえてfpsを1以下に下げる
                    //{
                    //    PlaneFinder.PlaneFindAsync(mappedDepthData, colorWidth, colorHeight).ContinueWith(t =>
                    //    {
                    //        var points = t.Result.Item2;
                    //        AverageDepth = (short)t.Result.Item2.Average(p => p.Z);
                    //        detectedPlaneViewbox.Dispatcher.BeginInvoke((Action)(() =>
                    //        {
                    //            var canvas = PlaneFinder.CreateDottedCanvas(colorWidth, colorHeight, points);
                    //            // draw rectangle around table
                    //            var rect = t.Result.Item1.PlaneSpace;
                    //            var rectangle = new System.Windows.Shapes.Rectangle() { Height = rect.Height, Width = rect.Width, StrokeThickness = 5, Stroke = Brushes.LightBlue };
                    //            rectangle.RenderTransform = new TranslateTransform(rect.Left, rect.Top);
                    //            canvas.Children.Add(rectangle);
                    //            detectedPlaneViewbox.Child = canvas;

                    //            detectedPlane = t.Result.Item1;
                    //        }));
                    //    });
                    //    //PlaneFinder.GetPlaneConvexHull(mappedDepthData, colorWidth, colorHeight).ContinueWith(t =>
                    //    //{
                    //    //    detectedPlaneViewbox.Dispatcher.BeginInvoke((Action)(() =>
                    //    //    {
                    //    //        detectedPlaneViewbox.Child = t.Result.Item1.GetConvexHullCanvas(colorWidth, colorHeight);
                    //    //    }));
                    //    //    AverageDepth = t.Result.Item2;
                    //    //});
                    //}

                    #endregion

                    if(avePlane == null){ 
                        avePlane = new PlaneFinder.AveragePlane();
                        avePlane.AverageFinished += s =>
                        {
                            this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                                // 机の色の設定
                                using (var deskImg = OpenCvUtility.Image.General.BgraToIplImage(colorBitmap))
                                {
                                    deskColor = DishFinder.GetDeskColorKmeans(deskImg, avePlane.ReliablePoints.Select(p => new CvPoint((int)p.X, (int)p.Y)));
                                    deskColorCanvas.Background = new SolidColorBrush(deskColor);
                                }

                                var canvas = detectedPlaneViewbox.Child != null ? detectedPlaneViewbox.Child as Canvas : new Canvas() { Width = colorWidth, Height = colorHeight };
                                detectedPlaneViewbox.Child = avePlane.DrawAveragePlane(canvas);
                            }));
                            AverageDepth = (short)avePlane.ReliablePoints.Average(p => p.Z);
                        };
                    }
                    if(avePlane.IsPlaneDetectEnough == false)
                        avePlane.FindPlane(mappedDepthData, colorWidth, colorHeight);

                    if(avePlane.IsAverageFinished){
                        var deskImg = OpenCvUtility.Image.General.BgraToIplImage(colorBitmap);
                        // 机の範囲を決める場合はノイズの影響を払うため、AveragePlaneを用いる。実際に皿を検出する場合は、ノイズの影響を含んだ状態でPlaneを検出したほうがよい
                        PlaneFinder.PlaneFindAsync(mappedDepthData, colorWidth, colorHeight, samplePlaneNum:1000)//  minDepth: (short)(AverageDepth - 20), maxDepth:(short)(AverageDepth + 20))
                        .ContinueWith(t =>
                        {
                            var detectedPlane = t.Result.Item1;
                            var deskRect = avePlane.AveragedPlane.PlaneSpace;
                            var detectedDishes = DishFinder.DetectDishLikeObjects(mappedDepthData, colorWidth, colorHeight, detectedPlane, deskRect, 10);

                            #region Skype login
                            preDetectedDishNum.Enqueue(detectedDishes.Count);
                            if (preDetectedDishNum.Count >= 7)
                            {
                                preDetectedDishNum.Dequeue();
                                var shouldLogin = preDetectedDishNum.Count(dishNum => dishNum > 0) >= 4; // 皿の数が１以上である状態が多数
                                if (isSkypeLoggedIn == false && shouldLogin)
                                {
                                    //皿が検出されるようになった
                                    var player = new System.Media.SoundPlayer(Properties.Resources.Windows_Hardware_Insert);
                                    player.Play();
                                    player.Dispose();
                                    sendSkypeCommand(@"--status login");
                                    isSkypeLoggedIn = true;
                                    // start new meal session
                                    mealSessionStartDateTime = currentTime;
                                }
                                if (isSkypeLoggedIn && shouldLogin == false)
                                {
                                    // 皿が検出されなくなった
                                    var player = new System.Media.SoundPlayer(Properties.Resources.Windows_Hardware_Remove);
                                    player.Play();
                                    player.Dispose();
                                    sendSkypeCommand(@"--status logout");
                                    isSkypeLoggedIn = false;
                                }
                                // 皿の数が最大になった場合は写真を記録する
                                if(detectedDishes.Count > maxDishNum){
                                    maxDishNum = detectedDishes.Count;
                                    var mealSessionDirPath = Path.Combine(foodLogImageSavingDirPath, mealSessionStartDateTime.ToString(Properties.Resources.DateTimeFormatText));
                                    // remove meal session data
                                    if (Directory.Exists(mealSessionDirPath)) Directory.Delete(mealSessionDirPath, true);
                                    Directory.CreateDirectory(mealSessionDirPath);
                                    using (var deskCroppedImg = OpenCvUtility.Image.General.CropImage(deskImg, OpenCvUtility.Image.General.ToCvRect(deskRect)))
                                    {
                                        var colorImgPath = Path.Combine(mealSessionDirPath, currentTime.ToString(Properties.Resources.DateTimeFormatText) + ".png");
                                        deskCroppedImg.SaveImage(colorImgPath);
                                    }
                                }
                            }
                            #endregion

                            if (interactionWindow != null)
                            {
                                switch (interactionState)
                                {
                                    case InteractionMode.ShowCalorie:
                                        var dishImgs = new List<IplImage>();
                                        foreach (var dish in detectedDishes)
                                        {
                                            var dishImg = OpenCvUtility.Image.General.CropImage(deskImg, OpenCvUtility.Image.General.ToCvRect(dish.BoundingRect));
                                            dishImgs.Add(dishImg);
                                        }
                                        interactionWindow.ShowCalories(detectedDishes, dishImgs);
                                        //interactionWindow.DrawDishes(detectedDishes);
                                        interactionState = InteractionMode.None;
                                        break;
                                    case InteractionMode.ShowFoodLog:
                                        // Get recent 3 food log image
                                        var recentFoodLogImgPaths = Directory.EnumerateFiles(foodLogImageSavingDirPath, "*.png", SearchOption.AllDirectories)
                                            .Where(imgPath => Path.GetDirectoryName(imgPath) != mealSessionStartDateTime.ToString(Properties.Resources.DateTimeFormatText)) // remove current session
                                            .OrderByDescending(imgPath => Path.GetDirectoryName(imgPath))
                                            .Take(3);
                                        interactionWindow.ShowFoodLog(recentFoodLogImgPaths);
                                        interactionState = InteractionMode.None;
                                        break;
                                    case InteractionMode.SearchFood:
                                    case InteractionMode.TransportFood:
                                        {
                                            #region Take Picture of specific position
                                            var foodCaptureArea = calcFoodCaptureArea(deskRect, detectedDishes);
                                            if (interactionWindow.IsTakingPicture == false)
                                            {
                                                interactionWindow.IsTakingPicture = true;
                                                var op = interactionWindow.CreateTakePictureTimer(foodCaptureArea);
                                                op.Completed += (s, e) =>
                                                {
                                                    countDownTimer = op.Result as DispatcherTimer;
                                                };
                                            }
                                            if (countDownTimer != null)
                                            {
                                                var tag = countDownTimer.Tag as Rect?;
                                                if (tag != null) //終わったらTagに値が入っている
                                                {
                                                    Console.WriteLine("Finish timer!");
                                                    interactionWindow.IsTakingPicture = false;
                                                    countDownTimer = null;
                                                    var roi = tag.Value;
                                                    using (var takenFoodImg = OpenCvUtility.Image.General.CropImage(deskImg, new CvRect((int)roi.Left, (int)roi.Top, (int)roi.Width, (int)roi.Height)))
                                                    {
                                                        if (interactionState == InteractionMode.SearchFood)
                                                        {

                                                        }
                                                        else if (interactionState == InteractionMode.TransportFood)
                                                        {
                                                            var nowStr = DateTime.Now.ToString(Properties.Resources.DateTimeFormatText);
                                                            takenFoodImg.SaveImage(Path.Combine(takenFoodImageSavingDirPath, nowStr + ".png"));
                                                        }
                                                    }
                                                    interactionState = InteractionMode.None;
                                                }
                                            }
                                        }
                                        #endregion
                                        break;
                                    case InteractionMode.ReceiveFood:
                                        {
                                            var foodCaptureArea = calcFoodCaptureArea(deskRect, detectedDishes);
                                            interactionWindow.ReceiveDishImage(foodCaptureArea);
                                            interactionState = InteractionMode.None;
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                interactionState = InteractionMode.None;
                            }
                            if (deskImg != null) deskImg.Dispose();
                        });
                    }
                }

                #endregion

                #region Classification
                if (startClassifyBodyPartsToggleButton.IsChecked.Value && classifier != null && classifier.isEstimating == false)
                {
                    Console.WriteLine(DateTime.Now.ToString(Properties.Resources.DateTimeFormatText) + "  calling estimation");
                    // use desk depth for body part
                    classifier.EstimateBodyPart(mappedDepthData, depthWidth, depthHeight, offsetVectors, maxDepth: AverageDepth).ContinueWith(t =>
                    {
                        var estimatedResultImage = t.Result;
                        bodyPartsEstimationBitmap = WriteableBitmapConverter.ToWriteableBitmap(estimatedResultImage);
                        estimatedResultImage.Dispose();
                    });
                }
                #endregion
                
                
                if (isRecording && (currentTime - prevRecordedTime).TotalMilliseconds > recordingInterval_ms)
                {
                    #region Save data
                    prevRecordedTime = currentTime;
                    // Saving Color image Data
                    var encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(this.colorBitmap));
                    var colorImgPath = Path.Combine(recordSessionDirPath, currentTime.ToString(Properties.Resources.DateTimeFormatText) + ".png");
                    using (var fs = new FileStream(colorImgPath, FileMode.Create)) encoder.Save(fs);

                    #endregion
                }
                prevRenderedTime = currentTime;
            }
        }

        #region Interactive Functions

        DispatcherTimer countDownTimer = null;

        static Rect calcFoodCaptureArea(Rect deskRect, List<DishFinder.Dish> dishes)
        {
            if (dishes.Count == 0) return new Rect();
            var widthMax = double.MinValue;
            var heightMax = double.MinValue;
            var leftMin = double.MaxValue; // careful that kinect image is reversing right and left
            Rect mostLeftDishArea = new Rect();
            foreach (var dish in dishes)
            {
                var dishArea = dish.BoundingRect;
                if (widthMax < dishArea.Width) widthMax = dishArea.Width;
                if (heightMax < dishArea.Height) heightMax = dishArea.Height;
                if (dishArea.Left < leftMin)
                {
                    leftMin = dishArea.Left;
                    mostLeftDishArea = dish.BoundingRect;
                }
            }
            var foodCaptureArea = new Rect(new Point(mostLeftDishArea.Left - widthMax, mostLeftDishArea.Bottom - 20), new Size(widthMax, heightMax));
            if (deskRect.Contains(foodCaptureArea) == false)
            {
                // if food capture area is out of desk, need to fix it
                foodCaptureArea = new Rect(new Point(foodCaptureArea.Left, deskRect.Bottom - foodCaptureArea.Height), foodCaptureArea.Size);
            }
            return foodCaptureArea;
        }

        static void searchFood(List<DishFinder.Dish> dishes)
        {
        }

        void sendSkypeCommand(string command)
        {
            var process = new Process();
            process.StartInfo.FileName = "SkypeConsole.exe";
            process.StartInfo.Arguments = command;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = true;
            process.Start();
        }
        
        #endregion


        private void showImagesCheckBox_Click(object sender, RoutedEventArgs e)
        {
            var checkbox = sender as System.Windows.Controls.CheckBox;
            if (checkbox == null) return;
            Image targetImage = null;
            if (checkbox.Name == showColorCheckBox.Name)
                targetImage = colorImage;
            else if (checkbox.Name == showBodyPartsCheckBox.Name)
                targetImage = bodyPartsEstimationImage;

            if (targetImage == null) return;
            if (checkbox.IsChecked.Value) 
                targetImage.Visibility = System.Windows.Visibility.Visible;
            else 
                targetImage.Visibility = System.Windows.Visibility.Hidden;
        }

        private void dataViewButton_Click(object sender, RoutedEventArgs e)
        {
            var dataViewWindow = new DataViewWindow(Properties.Settings.Default.AppDataRootPath);
            dataViewWindow.Show();
        }

        /// <summary>
        /// Handles the user clicking on the screenshot button
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void startRecordingToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (startRecordingToggleButton == null) return; // GUI is not initialized yet
            if (startRecordingToggleButton.IsChecked.Value) // Start Recording
            {
                isRecording = true;
                startRecordingToggleButton.Content = Properties.Resources.StopRecordingDataText;
                startRecordingToggleButton.Background = Brushes.Coral;

                var startRecordingSessionTime = DateTime.Now; // Time when recording has started
                var startRecordingSessionTimeStr = startRecordingSessionTime.ToString(Properties.Resources.DateTimeFormatText);
                recordSessionDirPath = Path.Combine(Properties.Settings.Default.AppDataRootPath, Properties.Resources.DirNameColorImages, startRecordingSessionTimeStr);
                Directory.CreateDirectory(recordSessionDirPath);
            }
            else // End Recording
            {
                isRecording = false;
                startRecordingToggleButton.Content = Properties.Resources.StartRecordingDataText;
                startRecordingToggleButton.Background = Brushes.LightGreen;
            }
        }

        private void selectRecordedDataExportPathButton_Click(object sender, RoutedEventArgs e)
        {
            var fbd = createDataExportPathDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                recordedDataExportPathTextBox.Text = fbd.SelectedPath;
                Properties.Settings.Default.AppDataRootPath = fbd.SelectedPath;
                Properties.Settings.Default.Save();
                startRecordingToggleButton.IsEnabled = true;
            }
        }

        FolderBrowserDialog createDataExportPathDialog()
        {
            var fbd = new FolderBrowserDialog();
            fbd.Description = "Choose Data Export Folder Path";
            if (Directory.Exists(Properties.Settings.Default.AppDataRootPath) == false)
            {
                fbd.SelectedPath = Properties.Settings.Default.AppDataRootPath;
            }
            return fbd;
        }

        #region Body parts classification
        private void loadClassifier()
        {
            var modelPath = Properties.Settings.Default.MainWindow_ImportModelPath;
            if (File.Exists(modelPath))
            {
                if (classifier != null) { classifier.Dispose(); }
                BodyPartsClassifier.LoadClassifier(modelPath).ContinueWith(t =>
                {
                    classifier = t.Result;
                    importingModelMessageLabel.Dispatcher.BeginInvoke(new Action(() => importingModelMessageLabel.Content = "Finished Loading Model"));
                });
                var learningSessionDirPath = Path.GetDirectoryName(modelPath);
                offsetVectors = Utility.ImportBinaryData<int[,]>(Path.Combine(learningSessionDirPath, Utility.OFFSET_VECTORS_FILE));
            }
            else
            {
                StatusText = "No model " + modelPath;
            }
        }

        private void selectImportingModelPathButton_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.Title = "Choose BodyPart extimation model path";
            ofd.Filter = string.Format("opencv model|*{0}|weka model|*{1}|All files (*.*)|*.*", Utility.CV_MODEL, Utility.WEKA_MODEL);
            if (File.Exists(Properties.Settings.Default.MainWindow_ImportModelPath) == false)
            {
                ofd.FileName = Properties.Settings.Default.MainWindow_ImportModelPath;
            }
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                importingModelPathTextBox.Text = ofd.FileName;
                Properties.Settings.Default.MainWindow_ImportModelPath = ofd.FileName;
                Properties.Settings.Default.Save();
                loadClassifier();
            }
        }

        #endregion

        private void openInteractionWindowButton_Click(object sender, RoutedEventArgs e)
        {
            if (interactionWindow != null) interactionWindow.Close();
            interactionWindow = new InteractionWindow();
            interactionWindow.Show();
        }


        #region Audio Related

        void initializeVoiceRecognition()
        {
            // grab the audio stream
            IReadOnlyList<AudioBeam> audioBeamList = this.kinectSensor.AudioSource.AudioBeams;
            System.IO.Stream audioStream = audioBeamList[0].OpenInputStream();

            // create the convert stream
            this.convertStream = new KinectAudioStream(audioStream);
            RecognizerInfo ri = TryGetKinectRecognizer();

            if (null != ri)
            {
                this.speechEngine = new SpeechRecognitionEngine(ri.Id);

                // Create a grammar from grammar definition XML file.
                using (var memoryStream = new MemoryStream(Encoding.ASCII.GetBytes(Properties.Resources.SpeechGrammar)))
                {
                    var g = new Grammar(memoryStream);
                    this.speechEngine.LoadGrammar(g);
                }

                this.speechEngine.SpeechRecognized += this.SpeechRecognized;
                this.speechEngine.SpeechRecognitionRejected += this.SpeechRejected;

                // let the convertStream know speech is going active
                this.convertStream.SpeechActive = true;

                // For long recognition sessions (a few hours or more), it may be beneficial to turn off adaptation of the acoustic model. 
                // This will prevent recognition accuracy from degrading over time.
                ////speechEngine.UpdateRecognizerSetting("AdaptationOn", 0);

                this.speechEngine.SetInputToAudioStream(
                    this.convertStream, new SpeechAudioFormatInfo(EncodingFormat.Pcm, 16000, 16, 1, 32000, 2, null));
                this.speechEngine.RecognizeAsync(RecognizeMode.Multiple);
            }
        }
        /// <summary>
        /// Gets the metadata for the speech recognizer (acoustic model) most suitable to
        /// process audio from Kinect device.
        /// </summary>
        /// <returns>
        /// RecognizerInfo if found, <code>null</code> otherwise.
        /// </returns>
        private static RecognizerInfo TryGetKinectRecognizer()
        {
            IEnumerable<RecognizerInfo> recognizers;

            // This is required to catch the case when an expected recognizer is not installed.
            // By default - the x86 Speech Runtime is always expected. 
            try
            {
                recognizers = SpeechRecognitionEngine.InstalledRecognizers();
            }
            catch (COMException)
            {
                return null;
            }

            foreach (RecognizerInfo recognizer in recognizers)
            {
                string value;
                recognizer.AdditionalInfo.TryGetValue("Kinect", out value);
                if ("True".Equals(value, StringComparison.OrdinalIgnoreCase) && "en-US".Equals(recognizer.Culture.Name, StringComparison.OrdinalIgnoreCase))
                {
                    return recognizer;
                }
            }

            return null;
        }

        void stopVoiceRecognition()
        {
            if (null != this.convertStream)
            {
                this.convertStream.SpeechActive = false;
            }
            if (null != this.speechEngine)
            {
                this.speechEngine.SpeechRecognized -= this.SpeechRecognized;
                this.speechEngine.SpeechRecognitionRejected -= this.SpeechRejected;
                this.speechEngine.RecognizeAsyncStop();
            }
        }

        /// <summary>
        /// Handler for recognized speech events.
        /// </summary>
        /// <param name="sender">object sending the event.</param>
        /// <param name="e">event arguments.</param>
        private void SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            // Speech utterance confidence below which we treat speech as if it hadn't been heard
            const double ConfidenceThreshold = 0.3;
            if (e.Result.Confidence >= ConfidenceThreshold)
            {
                switch (e.Result.Semantics.Value.ToString())
                {
                    case "CALORIE":
                        interactionState = InteractionMode.ShowCalorie;
                        break;

                    case "FOODLOG":
                        interactionState = InteractionMode.ShowFoodLog;
                        break;

                    case "SEARCH":
                        interactionState = InteractionMode.SearchFood;
                        break;

                    case "TRANSPORT":
                        interactionState = InteractionMode.TransportFood;
                        break;

                    case "RECEIVE":
                        interactionState = InteractionMode.ReceiveFood;
                        break;

                    //case "CALL":
                    //    sendSkypeCommand("--call masuda");
                    //    break;
                }
            }
            Console.WriteLine("Recognized " + interactionState.ToString());
        }

        /// <summary>
        /// Handler for rejected speech events.
        /// </summary>
        /// <param name="sender">object sending the event.</param>
        /// <param name="e">event arguments.</param>
        private void SpeechRejected(object sender, SpeechRecognitionRejectedEventArgs e)
        {
            Console.WriteLine("Failed to recognise!");
        }
        
        #endregion

    }
}
