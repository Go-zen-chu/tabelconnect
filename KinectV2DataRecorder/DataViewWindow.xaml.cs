﻿using System;
using System.ComponentModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace TabelConnect
{
    /// <summary>
    /// CheckDepthAndMarkerControl.xaml の相互作用ロジック
    /// </summary>
    public partial class DataViewWindow : Window, INotifyPropertyChanged
    {
        // INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        public event PropertyChangedEventHandler PropertyChanged;

        string recordDataRootPath;
        ushort[] depthRawData;
        private WriteableBitmap colorBitmap = null;
        private WriteableBitmap depthBitmap = null;

        #region Property
        string selectedImgPath;
        public string SelectedImgPath
        {
            get { return selectedImgPath; }
            private set
            {
                selectedImgPath = value;
                if (PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs("SelectedImgPath"));
                }
            }
        }

        string capturedDateTime;
        public string CapturedDateTime
        {
            get
            {
                if (capturedDateTime == null) return "";
                var dt = DateTime.ParseExact(capturedDateTime, Properties.Resources.DateTimeFormatText, null);
                return dt.ToString();
            }
            private set
            {
                capturedDateTime = value;
                if (PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs("CapturedDateTime"));
                }
            }
        }

        string sessionDateTime;
        public string SessionDateTime
        {
            get
            {
                if (sessionDateTime == null) return "";
                var dt = DateTime.ParseExact(sessionDateTime, Properties.Resources.DateTimeFormatText, null);
                return dt.ToString();
            }
            private set
            {
                sessionDateTime = value;
                if (PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs("SessionDateTime"));
                }
            }
        } 
        #endregion

        public DataViewWindow(string recordDataRootPath = null)
        {
            InitializeComponent();
            this.DataContext = this;
            this.recordDataRootPath = recordDataRootPath;
            initialize();
        }
        void initialize()
        {
            var ofd = new System.Windows.Forms.OpenFileDialog()
            {
                Title = "Select color image",
                Filter = "png files(*.png)|*.png",
            };
            if (recordDataRootPath != null && Directory.Exists(recordDataRootPath))
                ofd.InitialDirectory = Path.Combine(recordDataRootPath);

            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                SelectedImgPath = ofd.FileName;
                CapturedDateTime = Path.GetFileNameWithoutExtension(selectedImgPath);

                var parentDirPath = Path.GetDirectoryName(selectedImgPath);
                SessionDateTime = Path.GetFileName(parentDirPath);
                recordDataRootPath = Path.GetDirectoryName(Path.GetDirectoryName(parentDirPath));

                #region 画像の読み込み
                colorBitmap = new WriteableBitmap(new BitmapImage(new Uri(selectedImgPath)));
                colorImage.Source = colorBitmap;
                colorImageCheckBox.IsChecked = true;
                // 深さ画像
                depthImageCheckBox.IsEnabled = false;
                depthLabel.Content = "No Depth";
                #endregion
            }
        }

        private void imageCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var checkBoxName = (sender as CheckBox).Name;
            if (checkBoxName == colorImageCheckBox.Name) colorImage.Visibility = System.Windows.Visibility.Visible;
            else if (checkBoxName == depthImageCheckBox.Name) depthImage.Visibility = System.Windows.Visibility.Visible;
        }
        private void imageCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            var checkBoxName = (sender as CheckBox).Name;
            if (checkBoxName == colorImageCheckBox.Name) colorImage.Visibility = System.Windows.Visibility.Hidden;
            else if (checkBoxName == depthImageCheckBox.Name) depthImage.Visibility = System.Windows.Visibility.Hidden;
        }
        private void selectNewImageButton_Click(object sender, RoutedEventArgs e)
        {
            initialize();
        }

        private void imageViewBox_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // colorImage上の座標を取得できる
            var selectedPoint = e.GetPosition(colorImage);
            positionLabel.Content = string.Join(", ", selectedPoint.X.ToString("f2"), selectedPoint.Y.ToString("f2"));
            var pickedColor =  OpenCvUtility.Image.General.GetPickedColor(colorBitmap, selectedPoint);
            colorLabel.Content = pickedColor;
            if (depthBitmap != null && depthRawData != null)
            {
                depthColorLabel.Content = OpenCvUtility.Image.General.GetPickedColor(depthBitmap, selectedPoint);
                var depthIdx = (int)selectedPoint.X + (int)selectedPoint.Y * depthBitmap.PixelWidth;
                var depth = depthRawData[depthIdx];
                depthLabel.Content = depth + " mm";
            }
            if (selectHandColorToggleButton.IsChecked.Value)
            {
                handColorCanvas.Background = new SolidColorBrush(pickedColor);
            }
        }

        private void selectMappingAreaToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            if (colorBitmap == null) return;
            var mappingAreaSelector = new MappingAreaSelector() { Width = colorBitmap.PixelWidth, Height = colorBitmap.PixelHeight };
            mappingViewBox.Child = mappingAreaSelector;
        }

        private void selectMappingAreaToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            var mappingAreaSelector = mappingViewBox.Child as MappingAreaSelector;
            if (mappingAreaSelector != null)
            {
                mappingAreaSelector.SaveCornerPosition();
            }
            mappingViewBox.Child = null;
        }

        private void selectHandColorToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            var solidColorBrush = handColorCanvas.Background as SolidColorBrush;
            if (solidColorBrush != null)
            {
                Properties.Settings.Default.HandColor = solidColorBrush.Color;
                Properties.Settings.Default.Save();
            }
        }

    }
}
